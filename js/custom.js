function set_hero_background() {
  var backgrounds = [
    'img/hero_backgrounds/rock_climbing.jpg',
    'img/hero_backgrounds/group_photo_1.jpg',
    'img/hero_backgrounds/group_photo_2.jpg',
    'img/hero_backgrounds/convocation_1.jpg',
    'img/hero_backgrounds/convocation_2.jpg',
  ];
  var background_url = backgrounds[Math.floor(Math.random() * backgrounds.length)];
  $('header').css('background-image', "url('" + background_url + "')");
}

set_hero_background();
